# -*- coding: utf-8 -*-
"""
Created on Thu May 12 12:33:01 2016

@author: johnlakness
"""

import googlemaps
from transforms3d import euler
import math
import numpy as np
import operator as op
import functools as ft

SkynetElevationKey = "AIzaSyADcT_n_GROG3-riUOPdD56hqXsV--Gt80"
clla = [37.407928, -122.178655,500] #camera lat,lon,alt
cpyr = (-math.pi/4,0,0) #camera pitch, yaw, 
vref = [0,1,0] # reference vector (pointing north)
ifov = .1 # pixel field of view in radians
(xres,yres) = (640,480) # camera resolution
# get transformation from lat/lon to meters
de = 12.742e6 # diameter of earth
dm = (de*math.pi,math.cos(clla[0])*de,1)
cxyz = list(map(op.mul,clla,dm))
# get camera pointing vector
cpv = np.dot(euler.euler2mat(*cpyr),vref)
# get elevation directly under drone
gmaps = googlemaps.client.Client(key=SkynetElevationKey)
el = gmaps.elevation((clla[0],clla[1]))[0]['elevation']
# get intersection of flat plane and camera pointing vector
pxyz = [cxyz[0],cxyz[1],el] # plane normal origin
ppv = [0,0,1] #plane normal vector
def fnPlaneIntersect(Pp,Pv,Lp,Lv): 
    return list(map(op.add,Lp,map(ft.partial(op.mul,np.dot([p-c for p,c in zip(Pp,Lp)],Pv)/np.dot(Pv,Lv)),Lv)))
print(list(map(op.truediv,fnPlaneIntersect(pxyz,ppv,cxyz,cpv),dm)))
